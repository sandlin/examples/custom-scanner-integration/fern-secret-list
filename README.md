# Fern's Secret List

### **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

Contains a list of user passwords, clients, and keys. This project is used to showcase [how a custom security scanner can be intergrated into GitLab](https://docs.gitlab.com/ee/development/integrations/secure.html) and findings can be added to the [Pipeline Security Tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html), the [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/), and the [Merge Request Widget](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html).

**Note**: The vulnerability report and security merge request widget are only available on GitLab Ultimate.

## How it works

This project loads the [fern pattern scanner](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner) and runs it on all the files located in this project. You can see that the scanner is run by referencing the included remote template:

```yaml
include:
  - remote: https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/fern-pattern-scanner/-/raw/main/Jobs/.fern-pattern-scanner.gitlab-ci.yml
```

If you [import this project](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) to your group and follow the instructions below, you can see the vulnerabilities present in this project by checking out it's:

- [Pipeline Security tab](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/secret-list/-/pipelines/875154055/security)
- [Merge Request with Vulnerabilities](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/secret-list/-/merge_requests/1)
- [Vulnerability Report](https://gitlab.com/gitlab-de/tutorials/security-and-governance/custom-scanner-integration/secret-list/-/security/vulnerability_report)

## Viewing Vulnerabilities

In order to see vulnerabilities, you can do the following:

1. [Import this repo by URL](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) into your own group

2. [Run a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually) using the main branch.

3. Wait for the pipeline to complete

4. Click on the [Security tab](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html)

5. See all the detected vulnerabilities

6. Go to [Vulnerability Report](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)

7. See all the detected vulnerabilities

8. Create a merge request adding any **password** pattern to a file

9. Wait for the pipeline to run

10. See all the detected vulnerabilities in the [MR widget](https://docs.gitlab.com/ee/user/project/merge_requests/widgets.html)
